/* For this code to work the R9 and R10 0ohm resistors should be desoldered
 * since they link respectively PD0 and PB6 on one hand and PD1 and PB7 on the
 * other.
 *
 * Desoldering R13 might also be a good idea (it disconnects SW1 which grounds
 * PF4 if pressed by mistake, and we use PF4 to drive the ldoff signal)
 */

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/ustdlib.h"

struct tiva_pin {
    uint32_t port;
    uint8_t pin;
    // True if pin can be used as input
    bool in;
    // True if pin can be used as output
    bool out;
};

#define CDC_PIN(_n, _port, _pin, _in, _out) static const struct tiva_pin _n = { \
    .port = _port, \
    .pin = _pin, \
    .in = _in, \
    .out = _out, \
}

#define CDC_PIN_IN(_n, _port, _pin) CDC_PIN(_n, _port, _pin, true, false)
#define CDC_PIN_OUT(_n, _port, _pin) CDC_PIN(_n, _port, _pin, false, true)
#define CDC_PIN_INOUT(_n, _port, _pin) CDC_PIN(_n, _port, _pin, true, true)

// CXD1815Q address bus
CDC_PIN_OUT(decoder_addr0, GPIO_PORTA_BASE, GPIO_PIN_7);
CDC_PIN_OUT(decoder_addr1, GPIO_PORTA_BASE, GPIO_PIN_6);
CDC_PIN_OUT(decoder_addr2, GPIO_PORTA_BASE, GPIO_PIN_5);
CDC_PIN_OUT(decoder_addr3, GPIO_PORTB_BASE, GPIO_PIN_4);
CDC_PIN_OUT(decoder_addr4, GPIO_PORTE_BASE, GPIO_PIN_5);

// CXD1815Q data bus (bidirectional)
CDC_PIN_INOUT(decoder_dat0, GPIO_PORTE_BASE, GPIO_PIN_2);
CDC_PIN_INOUT(decoder_dat1, GPIO_PORTE_BASE, GPIO_PIN_3);
CDC_PIN_INOUT(decoder_dat2, GPIO_PORTA_BASE, GPIO_PIN_2);
CDC_PIN_INOUT(decoder_dat3, GPIO_PORTD_BASE, GPIO_PIN_0);
CDC_PIN_INOUT(decoder_dat4, GPIO_PORTD_BASE, GPIO_PIN_1);
CDC_PIN_INOUT(decoder_dat5, GPIO_PORTD_BASE, GPIO_PIN_2);
CDC_PIN_INOUT(decoder_dat6, GPIO_PORTD_BASE, GPIO_PIN_3);
CDC_PIN_INOUT(decoder_dat7, GPIO_PORTE_BASE, GPIO_PIN_1);

// CXD1815Q control signals
CDC_PIN_OUT(decoder_cs, GPIO_PORTB_BASE, GPIO_PIN_0);
CDC_PIN_OUT(decoder_wr, GPIO_PORTB_BASE, GPIO_PIN_1);
CDC_PIN_OUT(decoder_rd, GPIO_PORTE_BASE, GPIO_PIN_4);

// CXD2510Q serial interface (forwarded to servo)
CDC_PIN_OUT(dsp_dat, GPIO_PORTB_BASE, GPIO_PIN_6);
CDC_PIN_OUT(dsp_xlat, GPIO_PORTC_BASE, GPIO_PIN_7);
CDC_PIN_OUT(dsp_clk, GPIO_PORTD_BASE, GPIO_PIN_6);
CDC_PIN_OUT(dsp_sclk, GPIO_PORTB_BASE, GPIO_PIN_7);

// DSP SENS pin (input, can send various signals depending on the value in the
// CXD2510Q's shift register)
CDC_PIN_IN(dsp_sens, GPIO_PORTF_BASE, GPIO_PIN_1);

// SubQ input (serial link)
CDC_PIN_IN(dsp_subq,  GPIO_PORTC_BASE, GPIO_PIN_5);
CDC_PIN_OUT(dsp_sqck, GPIO_PORTC_BASE, GPIO_PIN_6);
// Pulsed by the CXD2510Q when SUBQ data can be read
CDC_PIN_IN(dsp_scor,  GPIO_PORTC_BASE, GPIO_PIN_4);

CDC_PIN_OUT(speed, GPIO_PORTA_BASE, GPIO_PIN_3);
CDC_PIN_OUT(mirror, GPIO_PORTA_BASE, GPIO_PIN_4);
CDC_PIN_OUT(ldoff, GPIO_PORTF_BASE, GPIO_PIN_4);
// LMTSW (POS0)
CDC_PIN_IN(lmtsw,  GPIO_PORTB_BASE, GPIO_PIN_5);

CDC_PIN_IN(button, GPIO_PORTF_BASE, GPIO_PIN_0);


void uart_send(const char *str, uint32_t count)
{
    while(count--)
    {
        uint8_t c = (uint8_t)*str++;

        if (c == '\n') {
            // Convert \n into \r\n
            MAP_UARTCharPut(UART0_BASE, '\r');
        }
        MAP_UARTCharPut(UART0_BASE, c);
    }
}

// Printf implementation using UART0
int printu(const char *fmt, ...) {
    static char print_buffer[256] = "";
    va_list ap;
    int r;

    va_start(ap, fmt);

    r = uvsnprintf(print_buffer, sizeof(print_buffer), fmt, ap);

    va_end(ap);

    if (r > 0) {
        uart_send(print_buffer, (uint32_t)r);
    }

    return r;
}

static void pin_set(const struct tiva_pin *pin, bool val) {
    HWREG(pin->port + (GPIO_O_DATA + (pin->pin << 2))) = val ? 0xff : 0;
}

static bool pin_get(const struct tiva_pin *pin) {
    return !!(HWREG(pin->port + (GPIO_O_DATA + (pin->pin << 2))));
}

static void pin_init(const struct tiva_pin *pin) {
    // In-out pins configured as input by default
    if (pin->in) {
        GPIOPinTypeGPIOInput(pin->port, pin->pin);
    } else if (pin->out) {
        // Output pins drive high by default
        GPIOPinTypeGPIOOutput(pin->port, pin->pin);
        pin_set(pin, 1);
    }
}

static void pin_in(const struct tiva_pin *pin) {
    if (!pin->in) {
        printu("Error: attempting to configure output pin as input\n");
        return;
    }

    HWREG(pin->port + GPIO_O_DIR) &= ~(pin->pin);
}

static void pin_out(const struct tiva_pin *pin, bool val) {
    if (!pin->out) {
        printu("Error: attempting to configure input pin as output\n");
        return;
    }

    HWREG(pin->port + GPIO_O_DIR) |= pin->pin;

    // Calling pin_set before switching the port to output doesn't seem to work
    // unfortunately, "writing to a pin configured as an inptu pin has no
    // effect".
    pin_set(pin, val);
}

static inline void nop(void) {
    __asm__ volatile ("nop");
}

// `d` volatile to prevent GCC from unrolling the loop (this way the time taken
// by this method should always grow roughly linearly with the value of d)
static inline void busy_delay(volatile unsigned d) {
    while (d--) {
        nop();
    }
}

static void decoder_write(uint8_t addr, uint8_t val) {
    pin_set(&decoder_addr0, addr & 1);
    pin_set(&decoder_addr1, (addr >> 1) & 1);
    pin_set(&decoder_addr2, (addr >> 2) & 1);
    pin_set(&decoder_addr3, (addr >> 3) & 1);
    pin_set(&decoder_addr4, (addr >> 4) & 1);

    pin_out(&decoder_dat0, val & 1);
    pin_out(&decoder_dat1, (val >> 1) & 1);
    pin_out(&decoder_dat2, (val >> 2) & 1);
    pin_out(&decoder_dat3, (val >> 3) & 1);
    pin_out(&decoder_dat4, (val >> 4) & 1);
    pin_out(&decoder_dat5, (val >> 5) & 1);
    pin_out(&decoder_dat6, (val >> 6) & 1);
    pin_out(&decoder_dat7, (val >> 7) & 1);

    pin_set(&decoder_cs, 0);
    pin_set(&decoder_wr, 0);

    nop();

    pin_set(&decoder_wr, 1);
    pin_set(&decoder_cs, 1);

    pin_in(&decoder_dat0);
    pin_in(&decoder_dat1);
    pin_in(&decoder_dat2);
    pin_in(&decoder_dat3);
    pin_in(&decoder_dat4);
    pin_in(&decoder_dat5);
    pin_in(&decoder_dat6);
    pin_in(&decoder_dat7);
}

static uint8_t decoder_read(uint8_t addr) {
    uint8_t r = 0;

    pin_set(&decoder_addr0, addr & 1);
    pin_set(&decoder_addr1, (addr >> 1) & 1);
    pin_set(&decoder_addr2, (addr >> 2) & 1);
    pin_set(&decoder_addr3, (addr >> 3) & 1);
    pin_set(&decoder_addr4, (addr >> 4) & 1);

    pin_set(&decoder_cs, 0);
    pin_set(&decoder_rd, 0);

    nop();
    nop();

    r |= pin_get(&decoder_dat0);
    r |= pin_get(&decoder_dat1) << 1;
    r |= pin_get(&decoder_dat2) << 2;
    r |= pin_get(&decoder_dat3) << 3;
    r |= pin_get(&decoder_dat4) << 4;
    r |= pin_get(&decoder_dat5) << 5;
    r |= pin_get(&decoder_dat6) << 6;
    r |= pin_get(&decoder_dat7) << 7;

    pin_set(&decoder_rd, 1);
    pin_set(&decoder_cs, 1);

    return r;
}

/// Send `nbits` to the CXD2510Q DSP's serial interface (also forwarded to the
//CXA1782BR). The data is sent LSB-first.
static void dsp_send(uint32_t v, unsigned nbits) {
    unsigned i;

    for (i = 0; i < nbits; i++) {
        pin_set(&dsp_clk, 0);
        pin_set(&dsp_dat, (v & 1) != 0);

        busy_delay(4);

        // Data is sampled on the rising edge of the clock
        pin_set(&dsp_clk, 1);

        busy_delay(3);

        v >>= 1;
    }
}


static void dsp_send8(uint8_t v) {
    dsp_send(v, 8);
}

static void dsp_send16(uint16_t v) {
    dsp_send(v, 16);
}

static void dsp_send20(uint32_t v) {
    dsp_send(v, 20);
}

static void dsp_send24(uint32_t v) {
    dsp_send(v, 24);
}

void dsp_latch(void) {
    pin_set(&dsp_xlat, 0);
    busy_delay(5);
    pin_set(&dsp_xlat, 1);
}

void dsp_send8_and_latch(uint8_t v) {
    dsp_send8(v);
    dsp_latch();
}

void dsp_send16_and_latch(uint16_t v) {
    dsp_send16(v);
    dsp_latch();
}

void dsp_send20_and_latch(uint32_t v) {
    dsp_send20(v);
    dsp_latch();
}

void dsp_send24_and_latch(uint32_t v) {
    dsp_send24(v);
    dsp_latch();
}

// Read from DSP using SENS and SCLK
uint8_t dsp_read8() {
    unsigned i;
    uint8_t r = 0;

    // We need to have at least 15us between XLAT and the read
    busy_delay(200);

    for (i = 0; i < 8; i++) {
        pin_set(&dsp_sclk, 0);
        busy_delay(2);

        r <<= 1;
        r |= pin_get(&dsp_sens);

        pin_set(&dsp_sclk, 1);
        busy_delay(2);
    }

    return r;
}

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void __error__(char *pcFilename, uint32_t ui32Line)
{
    printu("Library error: %s:%u\n", pcFilename, ui32Line);
}
#endif

bool dsp_read_sens(uint8_t r) {
    dsp_send(r, 4);

    busy_delay(3);

    return pin_get(&dsp_sens);
}

bool dsp_read_ov64(void) {
    return dsp_read_sens(0xe);
}

bool dsp_read_xbusy(void) {
    return dsp_read_sens(0x4);
}

bool dsp_read_fok(void) {
    return dsp_read_sens(0x5);
}

bool dsp_read_gfs(void) {
    return dsp_read_sens(0xa);
}

void init_cd_decoder(void) {
    // DECCTL -> AUTODIST
    decoder_write(0x3, 0x8);
    // DRVIF: 24BCLKs/WCLK, DATA strobed at posedge BCLK
    decoder_write(0x0, 0x28);
    // CONFIG1: RAM size 32Kw x 8b
    decoder_write(0x1, 0x40);
    // CONFIG2: DACODIS, attenuator and mixer activated for CD-DA
    decoder_write(0x2, 0x2);
    // CLRCTL: Clear busy, clear result, real-time ADPCM clear
    decoder_write(0xa, 0x70);
    // CLRINT: Clear all interrupts
    decoder_write(0xb, 0xff);
    // INTMASK: Enable: Drive overrun, decoder timeout, real-time ADPCM end,
    // host DMA complete, decoder interrupt and host command
    decoder_write(0x9, 0xde);

    // DADRC-L/M/H: 0, 0, 0
    decoder_write(0x10, 0);
    decoder_write(0x11, 0);
    decoder_write(0x12, 0);

    // DLADR-L/M/H: 0x23, 0x09, 0x00
    decoder_write(0x4, 0x23);
    decoder_write(0x5, 0x9);
    decoder_write(0x6, 0x0);

    // HXFR-L: 0
    decoder_write(0xc, 0);
    // HXFR-H: ??
    decoder_write(0xd, 0x40);
}

void mute_cd_da(void) {
    // CHPCTL: CD-DA mute
    decoder_write(0x7, 0x20);
}

void dsp_cancel_auto_sequence(void) {
    dsp_send16_and_latch(0x4000);

    busy_delay(100);
}

static void dsp_stop_spindle(void) {
    // Send 0xe0 to DSP (stop motor)
    dsp_send8_and_latch(0xe0);

    pin_set(&ldoff, 1);

    // Send 0x00 to servo: focus off, defect enable, search off, search down
    dsp_send8_and_latch(0x00);

    // Send 0x20 to servo: tracking off, sled off
    dsp_send8_and_latch(0x20);
}

static void dsp_configure_gain(void) {
    // DSP: TB, TP, Gain CLVS
    dsp_send8_and_latch(0xd7);
    // DSP: gain MDP0, gain DPS0
    dsp_send16_and_latch(0xc600);
}

void wait_for_button(void) {
    printu("Waiting for button press...\n");

    while (!pin_get(&button)) {
        ;
    }

    busy_delay(10000);

    while (pin_get(&button)) {
        ;
    }
}

void dsp_perform_avgr_measurements_and_compensation(void) {
    // RF zero level measurement
    dsp_send24_and_latch(0x380800);

    while (pin_get(&dsp_sens)) {
        ;
    }
    while (!pin_get(&dsp_sens)) {
        ;
    }

    // VC level measurement ON
    dsp_send24_and_latch(0x388000);

    while (pin_get(&dsp_sens)) {
        ;
    }
    while (!pin_get(&dsp_sens)) {
        ;
    }


    // Focus level measurement ON
    dsp_send24_and_latch(0x382000);

    while (pin_get(&dsp_sens)) {
        ;
    }
    while (!pin_get(&dsp_sens)) {
        ;
    }

    // Tracking zero level measurement ON
    dsp_send24_and_latch(0x380010);

    while (pin_get(&dsp_sens)) {
        ;
    }
    while (!pin_get(&dsp_sens)) {
        ;
    }

    // Apply compensation without DFCT
    dsp_send24_and_latch(0x38148a);
}

void read_subq(void) {
    unsigned byte;
    unsigned bit;
    uint8_t subq[12] = {0};

    for (byte = 0; byte < 12; byte++) {
        for (bit = 0; bit < 8; bit++) {
            pin_set(&dsp_sqck, 0);
            busy_delay(2);

            subq[byte] >>= 1;
            subq[byte] |= pin_get(&dsp_subq) << 7;

            pin_set(&dsp_sqck, 1);
            busy_delay(3);
        }
    }

    printu("SUBQ:");
    for (byte = 0; byte < 12; byte++) {
        printu(" %02x", subq[byte]);
    }
    printu("\n");
}

uint8_t read_soct(void) {
    unsigned i;
    uint8_t per = 0;
    uint8_t c1f = 0;
    uint8_t c2f = 0;
    bool fok;
    bool gfs;
    bool lock;
    bool emph;

    for (i = 0; i < 8; i++) {
        if (i > 0) {
            pin_set(&dsp_sqck, 0);
        }
        busy_delay(2);

        per |= pin_get(&dsp_subq) << i;

        pin_set(&dsp_sqck, 1);
        busy_delay(3);
    }

    for (i = 0; i < 3; i++) {
        pin_set(&dsp_sqck, 0);
        busy_delay(2);

        c1f |= pin_get(&dsp_subq) << i;

        pin_set(&dsp_sqck, 1);
        busy_delay(3);
    }

    for (i = 0; i < 3; i++) {
        pin_set(&dsp_sqck, 0);
        busy_delay(2);

        c2f |= pin_get(&dsp_subq) << i;

        pin_set(&dsp_sqck, 1);
        busy_delay(3);
    }

    pin_set(&dsp_sqck, 0);
    busy_delay(3);
    fok = pin_get(&dsp_subq);
    pin_set(&dsp_sqck, 1);
    busy_delay(4);

    pin_set(&dsp_sqck, 0);
    busy_delay(3);
    gfs = pin_get(&dsp_subq);
    pin_set(&dsp_sqck, 1);
    busy_delay(4);

    pin_set(&dsp_sqck, 0);
    busy_delay(3);
    lock = pin_get(&dsp_subq);
    pin_set(&dsp_sqck, 1);
    busy_delay(4);

    pin_set(&dsp_sqck, 0);
    busy_delay(3);
    emph = pin_get(&dsp_subq);
    pin_set(&dsp_sqck, 1);
    busy_delay(4);

    if (0) {
        printu("PER: 0x%02x C1F: 0x%x C2F: 0x%x FOK: %u GFS: %u LOCK: %u EMPH: %u\n",
               per, c1f, c2f, fok, gfs, lock, emph);
    }

    return per;
}

void wait_for_scor(void) {
    while ((GPIOIntStatus(dsp_scor.port, false) & dsp_scor.pin) == 0) {
        ;
    }

    GPIOIntClear(dsp_scor.port, dsp_scor.pin);
}

int calibrate_bias(void) {
    int bias;
    int best_bias = 0;
    unsigned best_per_avg = 0xffff;

    // Try to increase the bias to see if it improves
    for (bias = 0; bias < 100; bias += 10) {
        unsigned try;
        const unsigned ntry = 5;
        unsigned avg = 0;
        uint8_t per;

        printu("Bias %d\n", bias);

        dsp_send24_and_latch(0x34f400 | ((unsigned)bias & 0x3fe));

        for (try = 0; try < ntry; try++) {
            /* busy_delay(3000); */
            wait_for_scor();
            dsp_send16_and_latch(0x8120);
            per = read_soct();

            if (!dsp_read_fok()) {
                printu("lost focus!\n");
                return best_bias;
            }

            avg += per;

            /* printu(" 0x%02x (%u)", per, dsp_read_fok()); */
        }

        avg /= ntry;

        printu("PER avg: %u\n", avg);

        if (avg < best_per_avg) {
            best_bias = bias;
            best_per_avg = avg;
        } else {
            /* PER is worsening, give up */
            break;
        }
    }

    // Try to decrease the bias to see if it improves
    for (bias = -10; bias > -100; bias -= 10) {
        unsigned try;
        const unsigned ntry = 5;
        unsigned avg = 0;
        uint8_t per;

        printu("Bias %d\n", bias);

        dsp_send24_and_latch(0x34f400 | ((unsigned)bias & 0x3fe));

        for (try = 0; try < ntry; try++) {
            /* busy_delay(3000); */
            wait_for_scor();
            dsp_send16_and_latch(0x8120);
            per = read_soct();

            if (!dsp_read_fok()) {
                printu("lost focus!\n");
                return best_bias;
            }

            avg += per;

            /* printu(" 0x%02x (%u)", per, dsp_read_fok()); */
        }

        avg /= ntry;

        printu("PER avg: %u\n", avg);

        if (avg < best_per_avg) {
            best_bias = bias;
            best_per_avg = avg;
        } else {
            /* PER is worsening, give up */
            break;
        }
    }

    return best_bias;
}

void run(void) {
    int bias;

    printu("Init...\n");

    pin_init(&ldoff);
    pin_init(&lmtsw);

    pin_init(&dsp_dat);
    pin_init(&dsp_clk);
    pin_init(&dsp_xlat);
    pin_init(&dsp_sqck);

    pin_init(&dsp_sens);
    pin_init(&dsp_subq);
    pin_init(&dsp_scor);
    pin_init(&dsp_sclk);

    pin_init(&decoder_addr0);
    pin_init(&decoder_addr1);
    pin_init(&decoder_addr2);
    pin_init(&decoder_addr3);
    pin_init(&decoder_addr4);

    pin_init(&decoder_dat0);
    pin_init(&decoder_dat1);
    pin_init(&decoder_dat2);
    pin_init(&decoder_dat3);
    pin_init(&decoder_dat4);
    pin_init(&decoder_dat5);
    pin_init(&decoder_dat6);
    pin_init(&decoder_dat7);

    pin_init(&decoder_cs);
    pin_init(&decoder_wr);
    pin_init(&decoder_rd);

    // Button IO needs unlocking
    HWREG(button.port + GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(button.port + GPIO_O_CR) = 0x01;
    pin_in(&button);
    GPIOPadConfigSet(button.port,
                     button.pin,
                     GPIO_STRENGTH_2MA,
                     GPIO_PIN_TYPE_STD_WPU);

    // Monitor SCOR falling edge
    GPIOIntTypeSet(dsp_scor.port, dsp_scor.pin, GPIO_FALLING_EDGE);
    GPIOIntEnable(dsp_scor.port, dsp_scor.pin);

    // Pull-up on lmtsw
    GPIOPadConfigSet(lmtsw.port,
                     lmtsw.pin,
                     GPIO_STRENGTH_2MA,
                     GPIO_PIN_TYPE_STD_WPU);

    wait_for_button();

    //
    // Firmware init
    //

    // CLRCTL: Clear busy, clear result, real-time ADPCM clear, resync with DSP
    decoder_write(0xa, 0x71);

    init_cd_decoder();
    mute_cd_da();
    dsp_cancel_auto_sequence();
    dsp_stop_spindle();

    // Clear TRVSC
    dsp_send24_and_latch(0x34f000);

    // Focus bias OFF
    dsp_send24_and_latch(0x3a0000);

    // Focus bias data: 0
    dsp_send24_and_latch(0x34f400);

    // Focus bias ON
    dsp_send24_and_latch(0x3a4000);

    // Focus auto gain 0x30
    dsp_send24_and_latch(0x341330);

    // Tracking auto gain 0x30
    dsp_send24_and_latch(0x341330);

    // Sled auto gain 0x30
    dsp_send24_and_latch(0x340730);

    // Openartion for MIRR/DFCT/FOK
    dsp_send24_and_latch(0x340730);

    // Sled input gain: 0xe6
    dsp_send24_and_latch(0x3400e6);

    // Focus output gain: 0x4a
    dsp_send24_and_latch(0x34114a);

    // Tracking low boost filter A - low: 0x6f
    dsp_send24_and_latch(0x34114a);

    // Tracking low boost filter B - low: 0x6f
    dsp_send24_and_latch(0x341f64);

    // Tracking output gain: 0x20
    dsp_send24_and_latch(0x342220);

    // Focus gain output gain: 0x28
    dsp_send24_and_latch(0x342d28);

    // Tracking gain up output gain: 0x70
    dsp_send24_and_latch(0x343e70);

    // DSP: CD-ROM mode, enhanced anti-rolling, high-speed VCO
    dsp_send16_and_latch(0x8980);
    // DSP: Send 0xa0_40 to DSP: vari down
    dsp_send16_and_latch(0xa040);
    // DSP: normal speed playback, ASEQ on, Digital PLL
    dsp_send16_and_latch(0x9b00);

    dsp_configure_gain();

    // - Focus Zero Cross slice level: +250mV
    // - Sled move voltage: 9
    // - AGCNTL self-stop ON
    // - AGCNTL convergence completion judgment time: 63ms
    // - Focus AGCNTL internally generateed sine wave amplitude: large
    // - Tracking AGCNTL internally generated sine wave amplitude: small
    // - AGCNTL convergence sensitivity during high sensitivity adjustment: high
    // - AGCNTL convergence sensitivity during low sensitivity adjustment: low
    // - AGCNTL high sensitivity adjustment: ON
    // - AGCNTL high sensitivity adjustment time: 256ms
    dsp_send24_and_latch(0x3749aa);

    // SOCK to DA13, XOLT to DA12, SOUT to DA14
    dsp_send24_and_latch(0x3f0008);

    // Serial data read mode/select -> output RFDC input signal
    dsp_send16_and_latch(0x391e);

    // Reads 0x80
    printu("RFDC: 0x%02x\n", dsp_read8());

    //
    // Sequence called when shell closed
    //

    dsp_configure_gain();

    // DSP: normal speed playback, ASEQ on, Digital PLL
    dsp_send16_and_latch(0x9b00);

    // Set autodist
    decoder_write(0x3, 0x8);

    // SPEED low
    pin_set(&speed, 0);

    // DADRC-L/M/H: 0, 0, 0
    decoder_write(0x10, 0);
    decoder_write(0x11, 0);
    decoder_write(0x12, 0);

    // DECCTL:
    // - Enable drive last address
    // - Mode2, Form2
    // - Repeat correction mode
    decoder_write(0x3, 0xb6);

    // CLRCTL: Resync
    decoder_write(0xa, 0x1);

    // Set mirror high
    pin_set(&mirror, 1);

    // Send 0x11 to the DSP: Tracking control:
    // - Anti-shock OFF
    // - Brake OFF
    // - Tracking gain normal
    // - tracking gain up filter select 1
    dsp_send8_and_latch(0x11);

    // - Focus Zero Cross slice level: +250mV
    // - Sled move voltage: 9
    // - AGCNTL self-stop ON
    // - AGCNTL convergence completion judgment time: 63ms
    // - Focus AGCNTL internally generateed sine wave amplitude: large
    // - Tracking AGCNTL internally generated sine wave amplitude: small
    // - AGCNTL convergence sensitivity during high sensitivity adjustment: high
    // - AGCNTL convergence sensitivity during low sensitivity adjustment: low
    // - AGCNTL high sensitivity adjustment: ON
    // - AGCNTL high sensitivity adjustment time: 256ms
    dsp_send24_and_latch(0x3749aa);

    // Sled kick level: ±3
    dsp_send8_and_latch(0x32);

    // Move sled all the way back to the center
    while (pin_get(&lmtsw)) {
        // Servo tracking off, sled reverse mode
        dsp_send8_and_latch(0x23);
        busy_delay(10000);
    }

    // Servo tracking mode: Tracking off, sled off
    dsp_send8_and_latch(0x20);

    dsp_cancel_auto_sequence();
    dsp_stop_spindle();

    // Laser on
    pin_set(&ldoff, 0);

    // Focus search voltage down
    dsp_send8_and_latch(0x02);

    dsp_perform_avgr_measurements_and_compensation();

    wait_for_button();

    printu("auto-focus start\n");

    dsp_send16_and_latch(0x4700);

    // Wait for !busy
    dsp_read_xbusy();

    while (!pin_get(&dsp_sens)) {
    }

    if (!dsp_read_fok()) {
        printu("Focus failed!");
    }

    // Apply compensation with DFCT
    dsp_send24_and_latch(0x38140a);

    // CLVA
    dsp_send8_and_latch(0xe6);

    // Tracking control: anti-shock off, brake on,
    // tracking gain up filter select 1
    dsp_send8_and_latch(0x17);

    // Sled kick level: ±2
    dsp_send8_and_latch(0x31);

    // Tracking servo ON, sled servo ON
    dsp_send8_and_latch(0x25);

    printu("Before AGF\n");
    wait_for_button();

    // Apply compensation with DFCT and AGF (focus automatic gain adjustment)
    dsp_send24_and_latch(0x38160a);

    busy_delay(10000000);

    if (!pin_get(&dsp_sens)) {
        printu("AGF failed\n");
    }

    printu("Before AGT\n");

    // Apply compensation with DFCT and AGT (tracking automatic gain adjustment)
    dsp_send24_and_latch(0x38150a);

    busy_delay(10000000);

    if (!pin_get(&dsp_sens)) {
        printu("AGT failed\n");
    }

    dsp_send24_and_latch(0x38140a);

    // Tracking control: anti-shock off, brake off,
    // tracking gain up filter select 1
    dsp_send8_and_latch(0x13);

    // Sled kick level: ±2
    dsp_send8_and_latch(0x31);

    // Send 0x8120 to DSP: Mode setting
    // MODE setting: CDROM false, DOUT Mute false, D.out Mute-F false,
    // WSEL true, VCO SEL false, ASHS false, SOCT true
    dsp_send16_and_latch(0x8120);

    dsp_read_gfs();

    /* Focus bias data: 0x18 */
    dsp_send24_and_latch(0x34f418);


    /* XXX TODO: better focus calibration (try various bias values, use
     * read_soct to minimize PER) */
    bias = calibrate_bias();

    printu("Bias: %d\n", bias);

    // Focus bias OFF
    dsp_send24_and_latch(0x3a0000);

    // Focus bias data: -48
    dsp_send24_and_latch(0x34f400 | ((unsigned)bias & 0x3fe));

    // Focus bias ON
    dsp_send24_and_latch(0x3a4000);

    // Switch to CD-DA mode
    dsp_send16_and_latch(0x8180);
    // CHPCTL: CD-DA mode, mute
    decoder_write(0x7, 0x30);
    // DECCTL: CD-DA mode
    decoder_write(0x3, 0xf);

    // Switch to CD-ROM mode
    dsp_send16_and_latch(0x8980);
    // CHPCTL: CD-DA mute
    decoder_write(0x7, 0x20);
    // DECCTL: Real-time correction mode
    decoder_write(0x3, 0xd);

    printu("HERE\n");

    bool do_it = false;

    for (;;) {
        if (GPIOIntStatus(dsp_scor.port, false) & dsp_scor.pin) {
            uint8_t irq;

            GPIOIntClear(dsp_scor.port, dsp_scor.pin);

            // INTSTS
            irq = decoder_read(7);
            // CLRINT
            decoder_write(0xb, irq);

            if (!do_it)
                printu("INTSTS: %02x\n", irq);

            // Handle decoder timeout
            if (irq & 0x40) {

                // Switch to CD-DA mode
                dsp_send16_and_latch(0x8180);

                // DADRC-L/M/H: 0, 0, 0
                decoder_write(0x10, 0);
                decoder_write(0x11, 0);
                decoder_write(0x12, 0);

                // CHPCTL: CD-DA mode, mute
                decoder_write(0x7, 0x30);

                // DECCTL: CD-DA mode
                decoder_write(0x3, 0xf);
            }

            if (irq & 4) {
                // DECINT
                do_it = true;
            }

            /* read_subq(); */
        }
        if (do_it) {
            decoder_read(7);
        }
    }



    /* FS4 ON, no defect */
    /* dsp_send8_and_latch(0x0c); */

    printu("CLVA\n");
    dsp_send8_and_latch(0xe6);

    dsp_send8_and_latch(0x20);
    // Send 0x1d to DSP: tracking control: TG1, TG2 ON, brake OFF, sled kick ±2
    dsp_send8_and_latch(0x19);

    printu("pre-balance\n");

    // Calibration that seems to work for my drive: Gain 0, Balance 0. All
    // balance values seem to work as long as gain is 0.
    dsp_send8_and_latch(0x30);
    dsp_send8_and_latch(0x38);

    // Wait for calibration to complete
    while (!pin_get(&dsp_sens)) {
        ;
    }

    printu("CLVA\n");
    dsp_send8_and_latch(0xe6);

    // Send 0x1d to DSP: tracking control: TG1, TG2 ON, brake ON, sled kick ±2
    dsp_send8_and_latch(0x1d);

    // Send 0x25 to DSP: tracking ON, sled ON
    dsp_send8_and_latch(0x1d);

    // Send 0x08 to DSP: FS4 defect enable
    /* dsp_send8_and_latch(0x08); */

    printu("auto-focus start\n");
    dsp_send16_and_latch(0x4700);

    // Wait for !busy
    dsp_read_xbusy();

    while (!pin_get(&dsp_sens)) {
    }

    // DSP: CLVS

    // DSP: kick
    //dsp_send8_and_latch(0xe8);

#if 0
    /* busy_delay(4000000); */

    /* dsp_stop_spindle(); */

    /* busy_delay(20000000); */
#endif
}

int main(void)
{
    //
    // Enable lazy stacking for interrupt handlers.  This allows floating-point
    // instructions to be used within interrupt handlers, but at the expense of
    // extra stack usage.
    //
    MAP_FPUEnable();
    MAP_FPULazyStackingEnable();

    // Set the clocking to run from the PLL at 80 MHz.
    //
    MAP_SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                       SYSCTL_XTAL_16MHZ);

    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    //
    // Enable the peripherals used by this example.
    //
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    //
    // Set GPIO A1 as UART pin.
    //
    GPIOPinConfigure(GPIO_PA1_U0TX);
    MAP_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_1);

    //
    // Configure the UART for 115,200, 8-N-1 operation.
    //
    MAP_UARTConfigSetExpClk(UART0_BASE, MAP_SysCtlClockGet(), 115200,
                            (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                             UART_CONFIG_PAR_NONE));

    for (;;) {
        run();
    }
}
